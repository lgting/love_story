/**
 * Defines the API route we are using.
 */
module.exports = {
  outputDir: '../we_c/addons/love_story/template/mobile/dist',
  publicPath: process.env.NODE_ENV === 'production'
    ? '/addons/love_story/template/mobile/dist/'
    : '/',
    chainWebpack: config => {
      config.resolve.alias.set('vue', '@vue/compat')
  
      config.module
        .rule('vue')
        .use('vue-loader')
        .tap(options => {
          return {
            ...options,
            compilerOptions: {
              compatConfig: {
                MODE: 2
              }
            }
          }
        })
    }
  //配置跨域请求
  // devServer: {
  //   open: true,    //是否自动打开浏览器
  //   host: 'localhost',
  //   port: 8081,    //启动端口号
  //   https: false,    //是否开启https
  //   hotOnly: true,
  //   proxy: { // 配置跨域
  //     '': {
  //       target: 'http://linqiu.gysr.top/app/love_story_api.php',
  //       ws: true,
  //       changOrigin: true,    //是否开启代理
  //       pathRewrite: {
  //         '': ''
  //       }
  //     }
  //   },
  //   before: app => {}
  // },
}
