import Vue from 'vue'
import bless from "./modules/bless";
import invite_index from "./modules/invite_index";
import { createStore } from 'vuex'
import api from '@/api/setting'

export default createStore({
  state: {
    setting: {}
  },

  mutations: {
    setState(state, data) {
      state.setting = JSON.parse(data)
    },
  },

  actions: {
    async getSetting({ commit }, data) {
      await api.getSetting(data).then(res => {
        commit('setState', res.data.data)
      })
    },
  },
  modules: {
    bless,
    invite_index
  }
})

