import api from '../../api/wx'

const state = {
    wx_jdk_config: {}
}

const getters = {
}

const mutation = {
    set_wx_jdk: function ({ state },config) {
        state.wx_jdk_config = config
    }
}

const action = {
    //url: decodeURIComponent(device == "ios" ? window.entryUrl : authUrl) 微信的url为开始进入时的url 不会随着路由变化
    wx_auth: function ({ commit }, url) {
        let config = api.wx_jdk_config(url)
        commit('set_wx_jdk', config)
    }
}

export default {
    state,
    getters,
    mutation,
    action
}
