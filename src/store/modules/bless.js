import api from '../../api/bless'
export default {
  state: {
    send_message_status: 0,
    bless_list: [],
    gift_list: [],
    album_swiper:{},
    selected_gift:{
      index_0: 0,
      index_1: 0
    },
    page: 0,
    pullUpState: 1   // 子组件的pullUpState状态  1:上拉加载更多, 2:加载中……, 3:我是有底线的
  },

  getters: {
    get_select_gift (state) {
      let index_0 = state.selected_gift.index_0
      let index_1 = state.selected_gift.index_1
      return state.gift_list[ index_0 ][ index_1]
    }
  },
  mutations: {
    set_album(state, data) {
      state.album_swiper = data
    },
    set_status(state, data) {
      state.send_message_status = data
    },
    set_bless_list(state, data) {
      state.bless_list = data
    },
    set_load_bless_list(state, data) {
      data.forEach(function (val, index, data) {
        state.bless_list.push(val)
      })
      state.pullUpState = 1
    },
    set_gift_list(state, data) {
      state.gift_list = data
    },
    set_selected_gift(state, data) {
      state.selected_gift = {
        index_0: data.index_0,
        index_1: data.index_1
      }
    },
    gift_list_min_price: function (state, p) {
      let index_0 = p[0]
      let index_1 = p[1]
      let count = state.gift_list[index_0][index_1].count
      if(count>1){
        let price = state.gift_list[index_0][index_1].price / state.gift_list[index_0][index_1].count * (state.gift_list[index_0][index_1].count = --count)
        state.gift_list[index_0][index_1].price = Number.parseFloat(price).toFixed(2)
        state.send_gift =  state.gift_list[index_0][index_1]
      }else{
        return state.gift_list[index_0][index_1].disable_min = true
      }
    },
    gift_list_add_price: function (state, p) {
      let index_0 = p[0]
      let index_1 = p[1]
      let count = state.gift_list[index_0][index_1]['count']
      let price = state.gift_list[index_0][index_1]['price'] / state.gift_list[index_0][index_1]['count'] * (state.gift_list[index_0][index_1]['count'] = ++count)
      state.gift_list[index_0][index_1]['price'] = Number.parseFloat(price).toFixed(2)
      state.send_gift =  state.gift_list[index_0][index_1]
      return state.gift_list[index_0][index_1]['disable_min'] = false
    },

    set_page(state, page) {
      state.page = page
    },
    set_page_add(state) {
      state.page++
    },
    set_pull_up_state(state, val) {
      state.pullUpState = val
    }
  },

  actions: {
    send_message: function ({commit}, data) {
        return api.send_message(data)
    },
    gift_pay: function({state}, send_info) {
      return api.gift_pay(send_info)
    },
    send_gift_confirm: function({getters}) {
      return api.send_gift_confirm(getters.get_select_gift)
    },
    get_bless_list: function({commit,state}, action) {
      if(action == 'refresh'){
        commit('set_page', 0)
      }else {
        commit('set_page_add')
      }
       api.get_bless_list(state.page, action)
           .then(function (res) {
             if(action === 'refresh') {
               commit('set_pull_up_state',1)
               commit('set_bless_list', res.data.data)
             }else{
               if(res.data.data.length === 0){
                 commit('set_pull_up_state',3)
               }else{
                 commit('set_load_bless_list',res.data.data)
               }
             }
           })
           .catch(function () {
             console.log('列表获取失败')
           })
    },
    get_gift_list: function ({commit}) {
      api.get_gift_list()
          .then(function (res) {
            commit('set_gift_list', res.data.data)
          })
          .catch(function (error) {
            console.log(error)
          })
    },
    get_album_swiper: function ({commit}) {
      return api.get_album_swiper()
          .then(function (res) {
            commit('set_album', res.data.data)
            resolve()
          })
          .catch(function (error) {
            console.log(error)
            console.log('获取相册失败')
          })
    }
  }
}

