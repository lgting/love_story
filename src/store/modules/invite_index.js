import api from '../../api/invite_index'

const state = {
  countdown_text: [],
  invite_state: {}
}

const getters = {}

const mutations = {
  set_countdown: function (state, val) {
    state.countdown_text = val
  },
  set_invite_state: function (state, val) {
    state.invite_state = val
  }
}

const actions = {
  get_countdown_text: function ({commit}) {
    api.countdown_text()
        .then(function (res) {
          commit('set_countdown', res.data.data)
        })
  },
  get_invite_state: function ({commit}) {
    api.invite_state()
        .then(function (res) {
          commit('set_invite_state', res.data.data)
        })
  },
  invite_reply: function ({state}) {
    return api.invite_reply(state.invite_state)
  }
}



export default {
  state,
  getters,
  actions,
  mutations
}
