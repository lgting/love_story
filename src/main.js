import Vue from 'vue'
import App from './App.vue'
import router from './router/router'
import store from './store/'
import VueLazyload from 'vue-lazyload'
Vue.use(VueLazyload)

// 或者添加VueLazyload 选项
/*Vue.use(VueLazyload, {
  preLoad: 1.3,
  error: 'dist/error.png',
  loading: 'dist/loading.gif',
  attempt: 1
})*/
//import { configureCompat } from 'vue'



//Vue.config.productionTip = false
const app = Vue.createApp(App)
app.use(router)
app.use(store)
app.mount('#app')

