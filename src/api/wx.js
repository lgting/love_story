import c from './common'

export default {
    share_config: function(auth_url, router_name, router_path) {
        return c.axios.get(c.base_url + 'wx.index.share_config', {
            params: {
                auth_url,
                router_name,
                router_path
            }
        })
    },
    pay: function () {

    }
}
