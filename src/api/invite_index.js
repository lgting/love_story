import c from './common'
export default {
    countdown_text: () => {
      return c.axios
        .get(c.base_url + 'invite.index.countdown_text')
    },
    invite_state: () => {
      return c.axios
          .get(c.base_url + 'invite.index.invite_state')
    },
    invite_reply: (data) => {
        return c.axios({
            method: 'post',
            url: c.base_url + 'invite.index.invite_reply',
            data: {
                name: data.name,
                feast: data.feast,
                people_count: data.people_count,
                mobile: data.mobile
            }
        })
    }
}
