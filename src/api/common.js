import axios from 'axios'
import url from '../utils/url'

let i = url.getKey('i')
let username = url.getKey('username')

const base_url = process.env.VUE_APP_API_BASE_URL + '&i=' + i + '&username=' + username + '&r='

export default {
    axios,
    base_url
}
