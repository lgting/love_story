import c from './common'
export default {
    getSetting: (data) => {
        return c.axios({
            method: 'get',
            url: c.base_url + 'setting.index.getSetting',
            data,
        })
    },
    /* 
address: "海省西宁市湟源县申中乡俊家庄村"
days: "2"
file: ""
husbandMobile: "18917123313"
husbandName: "刘国庭"
inviteContent: "沉浸在幸福中的我们俩，将于2020年5月2日（四月初九）举办结婚典礼 敬备喜宴"
marryDate: "2020年5月20日"
shareDesc: "我们将在2020年5月2日举行婚礼典礼，请来分享我们婚礼的喜悦"
shareTitle: "我们要结婚啦！"
wifeMobile: "14782372952"
wifeName: "陈雪丽"
    
    */

}
