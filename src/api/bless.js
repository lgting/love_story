import c from './common'
export default {
    send_message: (data) => {
      return c.axios({
          method: 'post',
          url: c.base_url + 'bless.index.send_message',
          data: {
              name: data.name,
              content: data.content
          }
      })
    },

    gift_pay(send_info) {
        return c.axios({
            method: 'post',
            url: c.base_url + 'wxPay.index.gift_pay',
            data: send_info
        })
    },

    send_gift_confirm: (data) => {
        return c.axios({
            method: 'post',
            url: c.base_url + 'bless.index.send_gift_confirm',
            data: data
        })
    },

    get_bless_list: (page,action='refresh') => {

        return c.axios.get(c.base_url + 'bless.index.bless_list',{
            params: {
                page: page,
                action: action,
            }
        })

/*        return c.axios({
            method: 'post',
            url: c.base_url + 'bless.index.bless_list',
            data: {
                page: page,
                action: action,
            }
        })*/
    },

    get_gift_list: () => {
        return c.axios.get(c.base_url + 'bless.index.gift_list')
    },
    get_album_swiper: () => {
        return c.axios({
            method: 'post',
            url: c.base_url + 'bless.index.album_swiper',
            data: '获取相册'
        })
    }
}
