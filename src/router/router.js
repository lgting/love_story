import Vue from 'vue'
import { createRouter, createWebHashHistory } from 'vue-router'
import hook from './hook'
import wx from "../utils/wx";
import store from '../store';
//Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'inviteIndex',
    component: () => import('../views/InviteIndex.vue'),
    meta: { allowShare: true }
  },
  {
    path: '/inviteOpen',
    name: 'inviteOpen',
    component: () => import(/* webpackChunkName: "about" */ '../views/InviteOpen.vue'),
    meta: { allowShare: false }
  },
  {
    path: '/bless',
    name: 'bless',
    component: () => import(/* webpackChunkName: "about" */ '../views/BlessIndex.vue'),
    meta: { allowShare: false }
  },
  {
    path: '/witness',
    name: 'witness',
    component: () => import(/* webpackChunkName: "about" */ '../views/WitnessIndex.vue')
  }
]
/* const router = new VueRouter({
  routes
}) */

const router = createRouter({
  // 4. 内部提供了 history 模式的实现。为了简单起见，我们在这里使用 hash 模式。
  history: createWebHashHistory(),
  routes, // `routes: routes` 的缩写
})

//下面是官方的写法， 并不行
/* const routes = [
  { path: '/', component: Home },
  { path: '/about', component: About },
]

// 3. 创建路由实例并传递 `routes` 配置
// 你可以在这里输入更多的配置，但我们在这里
// 暂时保持简单
const router = VueRouter.createRouter({
  // 4. 内部提供了 history 模式的实现。为了简单起见，我们在这里使用 hash 模式。
  history: VueRouter.createWebHashHistory(),
  routes, // `routes: routes` 的缩写
})

// 5. 创建并挂载根实例
const app = Vue.createApp({}) */


//微信鉴权
router.afterEach((to, from) => {
  let auth_url = window.location.href
  hook.wx_config(auth_url, to.name, to.path)
});
export default router
