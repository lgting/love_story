import wx from "../utils/wx";
import wx_api from '../api/wx.js'

export default {
    wx_config: function(auth_url, router_name, router_path) {
        return wx_api.share_config(auth_url, router_name, router_path)
            .then((res)=>{
                let data = res.data.data
                if (!!window.__wxjs_is_wkwebview) {// IOS
                    console.log('ios')
                    wx(data.wx_jdk_config, data.share_config,);
                } else {
                    // 安卓
                    console.log('android')
                    setTimeout(function () {
                        wx(data.wx_jdk_config, data.share_config);
                    }, 500);
                }
            })

    }
}

