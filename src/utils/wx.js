import wx from 'weixin-js-sdk'

export default  (wx_jdk_config, share_config) => {
    let shareConfig = {
        title: share_config.title,
        desc: share_config.description,
        link: share_config.share_url,
        imgUrl: share_config.share_thumb
    };

    wx.config({
        debug: wx_jdk_config.debug,
        appId: wx_jdk_config.appId,
        timestamp: wx_jdk_config.timestamp,
        nonceStr: wx_jdk_config.nonceStr,
        signature: wx_jdk_config.signature,
        jsApiList: ["updateAppMessageShareData", "updateTimelineShareData", "onMenuShareAppMessage", "onMenuShareTimeline"]
    });

    wx.ready(() => {
        wx.updateAppMessageShareData({
            title: shareConfig.title,
            desc: shareConfig.desc,
            link: shareConfig.link,
            imgUrl: shareConfig.imgUrl,
            success: function () {//设置成功
                //shareSuccessCallback();
                console.log('updatAppMessageShareData success')
            }
        });
        wx.updateTimelineShareData({
            title: shareConfig.title,
            link: shareConfig.link,
            imgUrl: shareConfig.imgUrl,
            success: function () {//设置成功
                //shareSuccessCallback();
                console.log('updateTimelineShareData success')

            }
        });
        wx.onMenuShareTimeline({
            title: shareConfig.title,
            link: shareConfig.link,
            imgUrl: shareConfig.imgUrl,
            success: function () {
                //shareSuccessCallback();
                console.log('onMenuShareTimeline success')

            }
        });
        wx.onMenuShareAppMessage({
            title: shareConfig.title,
            desc: shareConfig.desc,
            link: shareConfig.link,
            imgUrl: shareConfig.imgUrl,
            success: function () {
                //shareSuccessCallback();
                console.log('onMenuShareAppMessage success')

            }
        });
    });
};

function shareSuccessCallback() {
    console.log('分享成功 回调函数')
}
