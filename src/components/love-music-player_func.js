export default {
    drawAudioArc: function ( audioCanvas ) {
        let audioCtx = audioCanvas.getContext('2d');
        audioCtx.translate(91, 91);
        audioCtx.strokeStyle = 'rgb(222,91,82)';
        audioCtx.lineWidth= '6';
        audioCtx.arc(0, 0, 80, 0, Math.PI*2, true);
        audioCtx.stroke();
        audioCtx.beginPath();
        audioCtx.lineWidth= '3';
        audioCtx.arc(0,0,68,Math.PI/6, Math.PI/6*4, false);
        audioCtx.stroke();
        audioCtx.beginPath();
        audioCtx.arc(0,0,68,Math.PI/6*9, Math.PI/6*6, true);
        audioCtx.stroke();
        audioCtx.beginPath();
        audioCtx.lineWidth= '2';
        audioCtx.arc(0,0,50,Math.PI/6*9, Math.PI/6*11, true);
        audioCtx.stroke();
    },
    drawAudioPlay: function(audioCanvasPlay) {
        let audioCtxPlay = audioCanvasPlay.getContext('2d');
        audioCtxPlay.translate(25, 10);
        audioCtxPlay.lineWidth= '4';
        audioCtxPlay.strokeStyle = 'rgb(222,91,82)';
        audioCtxPlay.rect(-10,0,20,20);
        audioCtxPlay.stroke();
        audioCtxPlay.beginPath();
        audioCtxPlay.rect(-10,20,20,10);
        audioCtxPlay.stroke();
        audioCtxPlay.beginPath();
        audioCtxPlay.moveTo(0,30);
        audioCtxPlay.lineTo(0,140);
        audioCtxPlay.stroke();
        audioCtxPlay.beginPath();
        audioCtxPlay.translate(0, 140);
        audioCtxPlay.rotate(120);
        audioCtxPlay.rect(0,0,18,32);
        audioCtxPlay.stroke();
        audioCtxPlay.beginPath();
        audioCtxPlay.lineWidth= '2';
        audioCtxPlay.moveTo(10,25);
        audioCtxPlay.lineTo(30,25);
        audioCtxPlay.stroke();
        audioCtxPlay.beginPath();
    },

    timeFormat: function (seconds) {
        var minite = Math.floor(seconds / 60);
        if(minite < 10) {
            minite = "0" + minite;
        }
        var second = Math.floor(seconds % 60);
        if(second < 10) {
            second = "0" + second;
        }
        return minite + ":" + second;
    },

    updateProgress: function(x){
        var progress = $('.audio-by-all');
        var position = x - progress.offset().left;
        var percentage = 100 * position / progress.width();
        if(percentage > 100) {
            percentage = 100;
        }
        if(percentage < 0) {
            percentage = 0;
        }
        $('.audio-by-now').css('width', percentage+'%');
        audio.currentTime = audio.duration * percentage / 100;
    },

    enableProgressDrag: function () {
            var progressDrag = false;
            $('.audio-by-all').on('mousedown', function(e) {
                progressDrag = true;
                updateProgress(e.pageX);
            });
            $(document).on('mouseup', function(e) {
                if(progressDrag) {
                    progressDrag = false;
                    updateProgress(e.pageX);
                }
            });
            $(document).on('mousemove', function(e) {
                if(progressDrag) {
                    updateProgress(e.pageX);
                }
            });
    },

    qiehuan: function (){
        if (audioIndex == audioName.length ){
            audioIndex = 0;
        }
        if (audioIndex == -1 ){
            audioIndex = audioName.length - 1;
        }
        audioText = new Array();
        audioText = audioName[audioIndex].split('-');
        $('.audio-head-tittle-text').text(audioText[1]);
        $('.audio-head-tittle-by').text(audioText[0]);
        $('.audio-head-tittle-text-out').text(audioText[1]);
        $('.audio-head-tittle-text-out-a').text(audioText[1]);
        $('.audio-head-tittle-text').css('text-indent', 0);
        audio.src = 'soures/' + audioName[audioIndex] + '.mp3';
        $('.audio-by-now').css('width', 0);
        if(audioPlay){
            audio.play();
        }
    },

    enableSoundDrag: function () {
    var volumeDrag = false;
    $('.audio-sound').on('mousedown', function(e) {
        volumeDrag = true;
        updateVolume(e.pageX);
    });
    $(document).on('mouseup', function(e) {
        if(volumeDrag) {
            volumeDrag = false;
            updateVolume(e.pageX);
        }
    });
    $(document).on('mousemove', function(e) {
        if(volumeDrag) {
            updateVolume(e.pageX);
        }
    });
},
        updateVolume: function (x, vol) {
        var volume = $('.audio-sound');
        var soundLeft = x - volume.offset().left;
        var percentage;
        if(vol) {
            percentage =vol * 100;
        } else {
            var position = soundLeft;
            percentage = 100 * position / volume.width();
        }
        if(percentage > 100) {
            percentage = 100;
        }
        if(percentage < 0) {
            percentage = 0;
        }
        percentage = parseInt(percentage);
        $('.audio-sound-now').css('width', percentage + '%');
        $('.audio-sound-art').css('left', percentage + '%');
        audio.volume = percentage / 100;
    },
    audioTextOut: function () {
        if(audioMax){
            var audioTextWidth = $('.audio-head-tittle-text-out-a').width();
            var audioTittleWidth = $('.audio-head-tittle-text').width();
        } else {
            var audioTextWidth = $('.audio-head-tittle-text-out').width();
            var audioTittleWidth = $('.audio-head-tittle-text').width();
        }
        if((audioTextWidth > audioTittleWidth) && !audioMax){
            if(outTextValue <= -(audioTextWidth - audioTittleWidth + 8)){
                audioSpeed = -audioSpeed;
            }
            if(outTextValue >= 8){
                audioSpeed = -audioSpeed;
            }
            outTextValue -= audioSpeed;
            var outText = outTextValue + 'px';
            $('.audio-head-tittle-text').css('text-indent', outText);
        }
        if((audioTextWidth > audioTittleWidth) && audioMax){
            if(outTextValue <= -(audioTextWidth - audioTittleWidth + 20)){
                audioSpeed = -audioSpeed;
            }
            if(outTextValue >= 20){
                audioSpeed = -audioSpeed;
            }
            outTextValue -= audioSpeed;
            var outText = outTextValue + 'px';
            $('.audio-head-tittle-text').css('text-indent', outText);
        }
    }
}
